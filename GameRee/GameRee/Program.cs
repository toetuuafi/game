﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameRee
{
    class Program
    {
        public static int width = 5;
        public static int height = 3;

        public static bool running = true;

        public struct Vector2
        {
            public int x;
            public int y;
        }

        public struct Entity
        {
            public Vector2 position;
            public string name;
            public string body;
        }

        public static List<Entity> allentities = new List<Entity>();

        static void Main()
        {
            Start();

            while (running) {
                Game();
            }
        }

        static void Start()
        {
            Entity Player = new Entity();
            Player.body = "H";
            Player.name = "Player";
            Player.position.x = 0;
            Player.position.y = height;

            allentities.Add(Player);
        }

        static void Game()
        {
            Entity Player = new Entity();
            for (int i = 0; i > allentities.Count; i++)
            {
                if (allentities[i].name == "Player")
                {
                    Player = allentities[i];
                    allentities.RemoveAt(i);
                }
            }
            Console.ReadKey();
            Player.position.x += 1;
            
            allentities.Add(Player);
            Render(Player.position.x);
        }

        static void Render(int ree)
        {
            Console.Clear();
            Console.WriteLine(ree);
            for (int y = 0; y < height; y++)
            {
                if (!(y == 0))Console.WriteLine("");
                for (int x = 0; x < width; x++)
                {
                    Vector2 pos = new Vector2();
                    pos.x = x;
                    pos.y = y;
                    string unit = " ";
                    foreach(Entity e in allentities)
                    {
                        if (e.position.x == pos.x) 
                        {
                            unit = e.body;
                            if (e.position.y == pos.y)
                            {
                                unit = e.body;
                            }
                        }
                    }
                    Console.Write(unit);
                }
            }
        }
    }
}
